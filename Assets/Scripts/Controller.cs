﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    //Range se utiliza para definir un rango en variables de tipo numerico
    //SerializeField permite mostrar las variables privadas en el editor de Unity
    [Range(0, 10), SerializeField] 
    private float speed = 6;

    //HideInInspector oculta las variables publicas en el editor de Unity
    [HideInInspector]
    public int n = 0;
    public float jump;
    private Rigidbody2D rigi;
    private Animator animator;


    //Este metodo se llama antes de iniciar la escena
    private void Awake()
    {
        Debug.Log("Awake");
        //Sirve para obtner el componente Rigibody2D, con este se puede obtener cualquier componente
        //que este disponible en el gameobject. Ejmp: <BoxCollider2D>, <SpriteRenderer>, etc.
        rigi = this.GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }


    //Este metodo se llama cuando inicia la escena
    void Start()
    {
        Debug.Log("Start");
    }

    // Update se llama cada frame (No utilizar físicas en este apartado) 
    void Update()
    {
#if NoPhysics
        //Input es para capturar el teclado, GetKey detecta cuando se este presionando la tecla
        if (Input.GetKey(KeyCode.RightArrow))
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        if (Input.GetKey(KeyCode.LeftArrow))
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        if (Input.GetKey(KeyCode.DownArrow))
            transform.Translate(Vector3.down * Time.deltaTime * speed);
        if (Input.GetKey(KeyCode.UpArrow))
            transform.Translate(Vector3.up * Time.deltaTime * speed);
#endif
    }


    //FixedUpdate se utiliza cuando se usan físicas, solo modificar las fisicas del juego en este apartado
    private void FixedUpdate()
    {
        //Se genera un Raycast desde la posicion del jugador con dirección hacia abajo
        //Que solo hace colision en la layer numero 8
        RaycastHit2D ray = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, 1 << 8);

        bool isGround = ray.distance < 0.5f && ray.distance > 0;


        if (Input.GetKey(KeyCode.RightArrow))
        {
            rigi.AddForce(Vector2.right * speed);
            animator.SetFloat("Walk", 1);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            rigi.AddForce(Vector2.left * speed);
            animator.SetFloat("Walk", 1);
        }
        else
        {
            animator.SetFloat("Walk", 0);
        }


        //Guardado temporal de datos mediante el teclado.
        if (Input.GetKey(KeyCode.S))
        {
            GameSave gameSave = GameObject.FindWithTag("GameSave").GetComponent<GameSave>();
            gameSave.Save();
        }

       
        //GetKey detecta cuando la tecla es presionada
        if (Input.GetKeyDown(KeyCode.Space) && isGround) 
            rigi.AddForce(Vector2.up * jump, ForceMode2D.Impulse);

    }
     

}

