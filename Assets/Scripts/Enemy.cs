﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Dynamic
{
    public float speedRot = 1;
    public float speed = 1;

    private Transform player;
    private Vector3 initPos;

    void Start()
    {
        // Busca con GameObject con un tag especifica, existe una alternativa para 
        // buscar con GameObject.Find() pero es mas deficiente
        player = GameObject.FindWithTag("Player").transform;
        initPos = this.transform.position;

        GameSave gameSave = GameObject.FindWithTag("GameSave").GetComponent<GameSave>();
        gameSave.RegisterGO(this.gameObject, "Enemy");
    }

    void Update()
    {
        // Hace una rotacón en el pivote del gameobject, al no estar en el FixedUpdate
        // Se debe multiplicar por el delta
        this.transform.Rotate(Vector3.forward * Time.deltaTime * speedRot);


        // Calcula la distacia que existe entre el enemigo y el jugador
        // si esta es menor a 5 unidades el enemigo persigue al jugador.
        if(Vector2.Distance(this.transform.position, player.position) < 5f)
        {
            this.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            Vector3 position = Vector3.Lerp(this.transform.position, player.position, speed);
            this.transform.position = position;
        }
        else
        {
            this.transform.localScale = Vector3.one;
            Vector3 position = Vector3.Lerp(this.transform.position, initPos, speed);
            this.transform.position = position;
        }

    }
}


