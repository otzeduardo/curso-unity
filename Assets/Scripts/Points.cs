﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : Dynamic
{
    public int value = 1;
    public bool canDestroy = true;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Player player = collision.GetComponent<Player>();
            player.points = player.points + value;
            if(canDestroy)
                Destroy(this.gameObject);
        }
    }
}
