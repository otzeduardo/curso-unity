﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class GenericObject 
{
    public Transform transform;
    public string type;
}
