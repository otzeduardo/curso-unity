﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : Dynamic
{
    public Sprite dead;
    public GameObject canvas;
    public TextMeshProUGUI score;

    private SpriteRenderer spriteRender;
    private Controller controller;
    private Rigidbody2D rigi;
    private Animator animator;
    private int nKeys = 0;
    public int points = 0;


    void Start()
    {
        spriteRender = this.GetComponent<SpriteRenderer>();
        controller = this.GetComponent<Controller>();
        rigi = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();

        GameSave gameSave = GameObject.FindWithTag("GameSave").GetComponent<GameSave>();
        gameSave.RegisterGO(this.gameObject, "Player");
    }


    private void Update()
    {
        if (transform.position.y < -10)
            GameOver();
        score.text = "Puntaje: " + points.ToString();

        if (points < 0)
            StartCoroutine(Dead());
    }

    // Si el player hace colisión con el enemigo el personaje muere.
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            StartCoroutine(Dead());
        }
    }


    // Coroutina que espera 5 segundos para despues eliminar el script de seguimiento de 
    // el personaje
    private IEnumerator Dead()
    {
        points = 0;
        Destroy(controller);

        //Al morir el personaje se agrega una fuerza hacia arriba para despues destruir el
        // componente del colider.
        rigi.AddForce(Vector3.up * 5, ForceMode2D.Impulse);
        Destroy(GetComponent<CapsuleCollider2D>());

        //el parametro isDead pasa a true para iniciar la animacion de Dead.
        animator.SetBool("isDead", true);

        yield return new WaitForSeconds(2f);

        //Obtiene el script de seguimiento de personaje para destruirla.
        CameraController cam = GameObject.FindWithTag("MainCamera").GetComponent<CameraController>();
        Destroy(cam);
        GameOver();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Key"))
        {
            Destroy(collision.gameObject);
            nKeys++;
        }
    }

    public int GetNKeys()
    {
        return nKeys;
    }

    public void RemoveKey()
    {
        nKeys--;
    }

    private void GameOver()
    {
        //Al morir el personaje se activa canvas de de GameOver
        canvas.SetActive(true);
        int lastScore = PlayerPrefs.GetInt("Score");

        if (points > lastScore)
            PlayerPrefs.SetInt("Score", points);

        TextMeshProUGUI score = canvas.transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        score.text = PlayerPrefs.GetInt("Score").ToString();

        score = canvas.transform.GetChild(4).GetComponent<TextMeshProUGUI>();
        score.text = points.ToString();
    }
}
