﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameSave : MonoBehaviour
{
    //Una lista para registar los GameObject que pueden guardarse
    private List<GenericObject> gObjects = new List<GenericObject>();
    private string _file = "/data.dat";


    /// <summary>
    /// /Registra un GameObject y lo guarda en la lista de gObjects.
    /// </summary>
    /// <param name="gObject">GameObject</param>
    /// <param name="type">Tipo de GameObject</param>
    public void RegisterGO(GameObject gObject, string type)
    {
        GenericObject genericObject = new GenericObject();
        genericObject.transform = gObject.transform;
        genericObject.type = type;

        gObjects.Add(genericObject);
    }



    /// <summary>
    /// Guarda los datos de los GameObjects en un archivo, localizado en la carpeta
    /// de StreamingAssets
    /// </summary>
    public void Save()
    {
        string[] data = new string[gObjects.Count];

        for(int i = 0; i < data.Length; i++)
        {
            string tmp = JsonUtility.ToJson(gObjects[i]);
            data[i] = tmp;
        }

        //Guarda un archivo por cada indice del array lo guarda en una linea del archivo
        File.WriteAllLines(Application.streamingAssetsPath + _file, data);
    }
}

