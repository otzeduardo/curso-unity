﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxKey : Dynamic
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if(player.GetNKeys() > 0)
            {
                player.RemoveKey();
                Destroy(this.gameObject);
            }
        }
    }
}
