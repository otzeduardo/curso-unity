﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform player;
    public float speed;

    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        Vector3 camPos = new Vector3(this.transform.position.x, this.transform.position.y, -10);
        Vector3 playerPos = new Vector3(player.position.x, player.position.y, -10);


        // La camara sigue al personaje utilizando un aintepolación lineal
        Vector3 pos = Vector3.Lerp(camPos, playerPos, speed);
        this.transform.position = pos;
    }
}
